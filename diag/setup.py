#!/usr/bin/python2

from distutils.core import setup
setup(name="ftdiagnose", version="0.5.4", py_modules=["ftdiagnose", "ftdiagnose_ui", "ftdiagnose_open_ui"])
